from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.loader.processor import TakeFirst
from scrapy.contrib.loader import XPathItemLoader
from scrapy.selector import HtmlXPathSelector
from scrapy.shell import inspect_response
from armani1.items import ArmaniItem

REGION = "USA"


class ArmaniLoader(XPathItemLoader):
    default_output_processor = TakeFirst()


class ArmaniSpider(CrawlSpider):
    name = "armani_usa"
    allowed_domains = ["www.armani.com/ua/armanicom"]
    # Стартуем прям с каталога
    start_urls = ["http://www.armani.com/subhome.asp?tskay=2E427F0A&gender=D&season=main"]

    rules = (
        Rule(LinkExtractor(
            # restrict_xpaths=["//*[@id='sidebarMenu']/ul/li[1]/ul"],
            allow=("www.armani.com/ua/armanicom/\w+/\w+/secondary\?salesline=\d+$")), follow=True),
        Rule(LinkExtractor(
            # restrict_xpaths=["//*[@id='elementsContainer']/div"],
            allow=("/\w*.html$")), callback='parse_item'),
    )

    # handle_httpstatus_list = [301, 302]

    def parse_item(self, response):
        print("STATUS:  ", response.status)
        # inspect_response(self, response)

        hxs = HtmlXPathSelector(response)
        l = ArmaniLoader(ArmaniItem(), hxs)
        # all = hxs.xpath()
        #

        l.add_xpath('region', "%s" % REGION)
        l.add_xpath('productName', '//*[@id="%s"]/article/aside/h1' % u'pageContent')
        l.add_xpath('currency', '//*[@id="%s"]/article/aside/div[3]/div[1]/span[1]/text()' % u'pageContent')
        l.add_xpath('priceValue', '//*[@id="%s"]/article/aside/div[3]/div[1]/span[2]/text()' % u'pageContent')
        l.add_xpath('color', '//*[@id="%s"]/a' % u'color_1')
        l.add_xpath('size', '//*[@id="%s"]/article/aside/div[4]/div[2]/a' % u'pageContent')
        l.add_xpath('descript', '//*[@id="%s"]/article/aside/ul[2]/li[1]/div[2]/ul' % u'pageContent')
        l.add_xpath('sku', '//*[@id="%s"]/article/aside/h3/span[2]' % u'pageContent')
        l.add_xpath('instock', '%s' % u'null')

        #
        l.add_xpath('id', '%s' % u"null")
        l.add_xpath('category', '%s' % u'null')
        l.add_xpath('scan_time', '%s' % u'null')

        l.add_value('url', response.url)

        return l.load_item()