import csv
import pandas

path = 'C:/Users/dvely/Desktop/IT/python/scrapy/armani1/scraped_data.csv'

# reader = csv.DictReader(open(path))
# for row in reader:
#     print(row['category;color;currency;descript;id;instock;priceValue;productName;region;size;sku;url'])

#OrderedDict([('category;color;currency;descript;id;instock;priceValue;productName;region;size;sku;url', 'null;Noir;currency;"Un accessoire passe-partout'), (None, [' iDimensionsВ\xa0: 39В\xa0xВ\xa029В\xa0xВ\xa05В\xa0cm.'])])

col_names = ['color', 'currency', 'region', 'size', 'descript']
#data = pandas.read_csv(filepath_or_buffer=path, sep=';', header=0, skip_blank_lines=True, usecols=col_names)
#print(data['region'])

def data_len(data):
    return (len(data.index))

class DataAnalysis():
    col_names = ['color', 'currency', 'region', 'size', 'descript']
    data = pandas.read_csv(filepath_or_buffer=path, sep=';', header=0, skip_blank_lines=True, usecols=col_names)
    data_c = 0
    row_count = data_len(data)
    print('All goods: ', row_count)

    def descript(self):
        row_count = self.row_count
        for i in range(0, row_count):
            if self.data['descript'][i] != 'NaN':
                self.data_c += 1

    def region_currency(self):
        # Number of objects
        data = self.data

        reg_true = 0
        reg_false = 0
        reg_null = 0
        for i in range(0, len(data['region'])):
            if data['currency'][i] == 'EUR' and data['region'][i] == 'FR':
                reg_true += 1
            elif data['currency'][i] == '$' and data['region'][i] == 'USA':
                reg_true += 1
            elif data['region'][i] == 'NaN' or '' or ' ':
                reg_null += 1
            else:
                reg_false += 1
        #print('TRUE: ', reg_true, '\nFALSE: ', reg_false)
        print(
              'Currency corresponds to the region:', reg_true,
              '\nCurrency does not match the region: ', reg_false,
              '\nGoods without region: ', reg_null,
              '\n--------------------------------------------'
              )

    def color(self):
        data = self.data
        colors_list = {}
        # Итераций в сумме
        for i in range(0, self.row_count):
            # Перебираем цвета позиции
            # shred_color - schrodinger's color. Цвет может быть, а может и не быть
            for schred_color in data['color'][i].split(','):
            # Перебираем уже добавленные цвета
                # Если уже есть такой цвет, плюсуем его количество
                if schred_color in colors_list:
                    colors_list[schred_color] += 1
                # Иначе добавляем цвет и "1" в значение
                else:
                    colors_list[schred_color] = 1
        print('Colors:')
        for color in colors_list:
            print(color, str(colors_list[color]/self.row_count*100) + '%')
        print('--------------------------------------------')

    def size(self):
        data = self.data
        sizes_list = {}
        # Итераций в сумме
        for i in range(0, self.row_count):
            # Перебираем цвета позиции
            # shred_color - schrodinger's color. Цвет может быть, а может и не быть
            for schred_size in data['size'][i].split(','):
            # Перебираем уже добавленные цвета
                # Если уже есть такой цвет, плюсуем его количество
                if schred_size in sizes_list:
                    sizes_list[schred_size] += 1
                # Иначе добавляем цвет и "1" в значение
                else:
                    sizes_list[schred_size] = 1
        print('Size:')
        for size in sizes_list:
            print(size, str(sizes_list[size]/self.row_count*100) + '%')


def main():
    da = DataAnalysis()
    da.region_currency()
    da.descript()
    da.color()
    da.size()

if __name__ == '__main__':
    main()
    #pass


#data_analysis(data)
