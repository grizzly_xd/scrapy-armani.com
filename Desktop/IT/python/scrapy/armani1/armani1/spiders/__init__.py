# This package will contain the spiders of your Scrapy project
#
# Scrapy crowler spider.
# Target: http://www.armani.com
#
# Author: Velychko Danylo
# E-mail: grizzly.drum@gmail.com

# Run command: scrapy crawl armani_spider --set FEED_URI=scraped_data.csv --set FEED_FORMAT=mycsv
# Delimiter in scraped_data.csv: ';'
#
# Requests for addresses of some categories returned 301.
# Engine Scrapy did not always adequately fulfill in connection with what,
# had to do "crutches" in the form of a two-stage request.
# For example:
# valid_url = Request(url=url)
# Request(url=valid_url.url, callback=self.parse_category)
