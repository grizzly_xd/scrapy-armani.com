'''
Run command: scrapy crawl armani_spider --set FEED_URI=scraped_data.csv --set FEED_FORMAT=mycsv
REGION France: "http://www.armani.com/fr"
REGION United States: "http://www.armani.com/us"
'''
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.loader.processor import TakeFirst
from scrapy.contrib.loader import XPathItemLoader
from armani1.items import ArmaniItem
from scrapy import Request
from scrapy import Selector
import re

# import scrapy
# from twisted.internet import reactor
# from scrapy.crawler import CrawlerRunner
# from scrapy.utils.log import configure_logging

DEFAULT_URL = "http://www.armani.com"


class ArmaniLoader(XPathItemLoader):
    default_output_processor = TakeFirst()


class ArmaniSpider(CrawlSpider):
    isWoman = bool
    categorys_list = []
    name = "armani_spider_fr"
    allowed_domains = ["www.armani.com"]
    start_urls = ["http://www.armani.com/fr", "http://www.armani.com/us"]

    # rules = (
    #     Rule(LinkExtractor(
    #             #restrict_xpaths=["//*[@id='sidebarMenu']/ul/li[1]/ul"],
    #             allow=("www.armani.com/ua/armanicom/\w+/\w+/secondary\?salesline=\d+$")), follow=True),
    #     Rule(LinkExtractor(
    #             #restrict_xpaths=["//*[@id='elementsContainer']/div"],
    #             allow=("http://www.armani.com/\w+/armanicom/\w+.html$")), callback='parse_item'),
    # )

    def start_requests(self):
        # urls = [
        #     'http://www.armani.com/subhome.asp?tskay=2E427F0A&gender=D&season=main',
        #     'http://www.armani.com/subhome.asp?tskay=6A2DD123&gender=D&season=secondary',
        #     'http://www.armani.com/subhome.asp?tskay=DA1D4DDC&gender=D&season=secondary',
        #     'http://www.armani.com/ua/armanicom/women/llprdctsea7/secondary',
        #     'http://www.armani.com/subhome.asp?tskay=2E427F0A&gender=U&season=main',
        #     'http://www.armani.com/subhome.asp?tskay=6A2DD123&gender=U&season=main',
        #     'http://www.armani.com/subhome.asp?tskay=DA1D4DDC&gender=U&season=main',
        #     'http://www.armani.com/ua/armanicom/men/llprdctsea7/secondary',
        # ]
        yield Request(url=self.start_urls[0],
                      callback=self.parse_sex_category)
        yield Request(url=self.start_urls[1],
                      callback=self.parse_sex_category)

    def parse_sex_category(self, response):
        # xpath_woman_clothing_lines = response.xpath('//*[@id="subMenuBar"]/div/div/ul[1]/li[1]/a')

        count_woman_clothing_lines = int(float(response.xpath('count(//*[@id="subMenuBar"]/div/div/ul[1]/li)').
                                               extract_first()))
        for clothing_line_number in range(1, count_woman_clothing_lines+1):
            self.isWoman = True
            xpath_woman_clothing_line = response.xpath('//*[@id="subMenuBar"]/div/div/ul[1]/li[%s]/a' %
                                                       clothing_line_number)
            url_woman_clothing_line = xpath_woman_clothing_line.css('a::attr(href)').extract_first()
            yield Request(url=url_woman_clothing_line, callback=self.parse_main_category)

        count_man_clothing_lines = int(float(response.xpath('count(//*[@id="subMenuBar"]/div/div/ul[2]/li)').
                                             extract_first()))
        for clothing_line_number in range(1, count_man_clothing_lines+1):
            self.isWoman = False
            xpath_man_clothing_line = response.xpath('//*[@id="subMenuBar"]/div/div/ul[2]/li[%s]/a' %
                                                     clothing_line_number)
            url_man_clothing_line = xpath_man_clothing_line.css('a::attr(href)').extract_first()
            yield Request(url=url_man_clothing_line, callback=self.parse_main_category)

    # Parse ('sidebarMenu')
    def parse_main_category(self, response):
        #Кондишен будет True тогда, когда в response будет женская категория.
        rege = re.match(r'http://www.armani.com/subhome.asp\?tskay=\w+&gender=D&season=\w+$', response.url)
        if str(rege) != "None":
            ul_numb = 1
        else:
            ul_numb = 2
        with open('debug_log.txt', 'a')as f:
            f.write(str("HERE ") + str(ul_numb) + '\n'+str(response.url) + '\n'+str(rege)+'\n'+'\n')
        main_category_list = []
        # Тут проверяем, есть ли на главной страници модельной линии (кутюрье) итемы.
        # Если да, отправляем сразу на парсинг итемов.
        nonSubCategory_items_count = int(float(response.xpath('count(//*[@id="elementsContainer"]/div/a)')
                                               .extract_first()))
        with open('debug_log.txt', 'a')as f:
            f.write(str(response.url) + ' ' + str(nonSubCategory_items_count) + '\n')
        if nonSubCategory_items_count != 0:
            valid_url = Request(url=response)
            with open('items_link.txt', 'a') as f:
                f.write(valid_url.url + '\n')
            yield Request(url=valid_url.url, callback=self.parse_item)

        main_category_count = response.xpath('count(//*[@id="sidebarMenu"]/ul/li[%s]/ul/li)' % ul_numb).extract_first()
        for number in range(1, int(float(main_category_count))+1):
            sub_sub_count = int(float(response.xpath("count(//*[@id='sidebarMenu']/ul/li[%s]/ul/li[%s]/ul/li)" %
                                                     (ul_numb, number)).extract_first()))
            if sub_sub_count > 1:
                for sub_category in range(1, sub_sub_count+1):
                    sub_menu = response.xpath('//*[@id="sidebarMenu"]/ul/li[%s]/ul/li[%s]/ul/li[%s]/a' %
                                              (ul_numb, number, sub_category))
                    url = sub_menu.css('a::attr(href)').extract_first()
                    if url[0] == "/":
                        url = DEFAULT_URL + url
                    valid_url = Request(url=url)
                    with open('categorys.txt', 'a') as f:
                        f.write(valid_url.url + '\n')
                    yield Request(url=valid_url.url, callback=self.parse_category)
            sub_menu = response.xpath('//*[@id="sidebarMenu"]/ul/li[%s]/ul/li[%s]/a' % (ul_numb, number))
            url = sub_menu.css('a::attr(href)').extract_first()
            if url[0] == "/":
                url = DEFAULT_URL+url
            self.categorys_list.append(url)
            # Тут походу искуственный редирект. А не просто 301.
            valid_url = Request(url=url)
            with open('categorys.txt', 'a') as f:
                f.write(valid_url.url + '\n')
            main_category_list.append(valid_url)
            yield Request(url=valid_url.url, callback=self.parse_category)

    # Parse Category. The end is near
    def parse_category(self, response):
        last_category_list = []
        item_count = response.xpath('count(//*[@id="elementsContainer"]/div)').extract_first()
        for number in range(2, int(float(item_count))+1):
            item_xpath = response.xpath('//*[@id="elementsContainer"]/div[%s]' % number)
            item = item_xpath.css('a::attr(href)').extract_first()
            if item[0] == "/":
                item = DEFAULT_URL + item
            last_category_list.append(item)
            valid_url = Request(url=item)
            with open('items_link.txt', 'a') as f:
                f.write(valid_url.url + '\n')
            yield Request(url=valid_url.url, callback=self.parse_item)

    def parse_nonSubCategory(self, response, items_count):
        for item in range(2, items_count):
            item_xpath = response.xpath('//*[@id="elementsContainer"]/div[%s]' % item)
            item_url = item_xpath.css('a::attr(href)').extract_first()
            if item_url[0] == "/":
                item = DEFAULT_URL + item_url
            valid_url = Request(url=item)
            with open('debug_log.txt', 'a')as f:
                f.write(valid_url)
            yield Request(url=valid_url.url, callback=self.parse_item)

    def parse_item(self, response):
        size_list_xpath = ["//*[@id='sizew_1']/a/text()", "//*[@id='sizew_2']/a/text()", "//li[@id='sizew_3']/a/text()",
                           "//li[@id='sizew_4']/a/text()", "//*[@id='sizew_5']/a/text()", "//*[@id='sizew_6']/a/text()"]
        color_list_xpath = ["//*[@id='color_1']/a/text()", "//*[@id='color_2']/a/text()", "//*[@id='color_3']/a/text()",
                            "//*[@id='color_4']/a/text()", "//*[@id='color_5']/a/text()", "//*[@id='color_6']/a/text()"]
        hxs = Selector(response)
        l = ArmaniLoader(ArmaniItem(), hxs)
        currency = response.xpath('//*[@id="pageContent"]/article/aside/div[3]/div[1]/span[1]/text()').extract_first()
        # http://www.armani.com/fr/armanicom/sac-besace_cod45369108hd.html
        region = None
        if re.match(r'http://www.armani.com/fr/\S+$', response.url):
            region = 'FR'
        elif re.match(r'http://www.armani.com/us/\S+$', response.url):
            region = 'USA'
        l.add_value('region', region)
        l.add_value('currency', currency)
        l.add_xpath('productName', '//*[@id="%s"]/article/aside/h1/text()' % u'pageContent')
        l.add_xpath('priceValue', '//*[@id="%s"]/article/aside/div[3]/div[1]/span[2]/text()' % u'pageContent')
        colors_list = []
        for color in color_list_xpath:
            color_value = response.xpath(color)
            if len(str(color_value)) > 5:
                colors_list.append(color_value.extract_first())

        l.add_value('color', ','.join(colors_list))
        # l.add_xpath('size', '//*[@id="%s"]/li/preceding-sibling::*' % u'elm_5')
        l.add_xpath('descript', '//*[@id="%s"]/article/aside/ul[2]/li[1]/div[2]/text()' % u'pageContent')
        l.add_xpath('sku', '//*[@id="%s"]/article/aside/h3/span[2]/text()' % u'pageContent')
        #
        # l.add_value('id', '%s' % u"null")
        # l.add_value('category', '%s' % u'null')

        final_size_list = []
        for size in size_list_xpath:
            size_value = response.xpath(size)
            if len(str(size_value)) > 5:
                final_size_list.append(size_value.extract_first())
        l.add_value('size', ','.join(final_size_list))
        l.add_value('url', response.url)
 
        return l.load_item()

class ArmaniSpider2(CrawlSpider):
    isWoman = bool
    categorys_list = []
    name = "armani_spider_us"
    allowed_domains = ["www.armani.com"]
    start_urls = ["http://www.armani.com/fr", "http://www.armani.com/us"]

    # rules = (
    #     Rule(LinkExtractor(
    #             #restrict_xpaths=["//*[@id='sidebarMenu']/ul/li[1]/ul"],
    #             allow=("www.armani.com/ua/armanicom/\w+/\w+/secondary\?salesline=\d+$")), follow=True),
    #     Rule(LinkExtractor(
    #             #restrict_xpaths=["//*[@id='elementsContainer']/div"],
    #             allow=("http://www.armani.com/\w+/armanicom/\w+.html$")), callback='parse_item'),
    # )

    def start_requests(self):
        # urls = [
        #     'http://www.armani.com/subhome.asp?tskay=2E427F0A&gender=D&season=main',
        #     'http://www.armani.com/subhome.asp?tskay=6A2DD123&gender=D&season=secondary',
        #     'http://www.armani.com/subhome.asp?tskay=DA1D4DDC&gender=D&season=secondary',
        #     'http://www.armani.com/ua/armanicom/women/llprdctsea7/secondary',
        #     'http://www.armani.com/subhome.asp?tskay=2E427F0A&gender=U&season=main',
        #     'http://www.armani.com/subhome.asp?tskay=6A2DD123&gender=U&season=main',
        #     'http://www.armani.com/subhome.asp?tskay=DA1D4DDC&gender=U&season=main',
        #     'http://www.armani.com/ua/armanicom/men/llprdctsea7/secondary',
        # ]
        #yield Request(url=self.start_urls[0],
        #              callback=self.parse_sex_category)
        yield Request(url=self.start_urls[1],
                      callback=self.parse_sex_category)

    def parse_sex_category(self, response):
        # xpath_woman_clothing_lines = response.xpath('//*[@id="subMenuBar"]/div/div/ul[1]/li[1]/a')

        count_woman_clothing_lines = int(float(response.xpath('count(//*[@id="subMenuBar"]/div/div/ul[1]/li)').
                                               extract_first()))
        for clothing_line_number in range(1, count_woman_clothing_lines + 1):
            self.isWoman = True
            xpath_woman_clothing_line = response.xpath('//*[@id="subMenuBar"]/div/div/ul[1]/li[%s]/a' %
                                                       clothing_line_number)
            url_woman_clothing_line = xpath_woman_clothing_line.css('a::attr(href)').extract_first()
            yield Request(url=url_woman_clothing_line, callback=self.parse_main_category)

        count_man_clothing_lines = int(float(response.xpath('count(//*[@id="subMenuBar"]/div/div/ul[2]/li)').
                                             extract_first()))
        for clothing_line_number in range(1, count_man_clothing_lines + 1):
            self.isWoman = False
            xpath_man_clothing_line = response.xpath('//*[@id="subMenuBar"]/div/div/ul[2]/li[%s]/a' %
                                                     clothing_line_number)
            url_man_clothing_line = xpath_man_clothing_line.css('a::attr(href)').extract_first()
            yield Request(url=url_man_clothing_line, callback=self.parse_main_category)

    # Parse ('sidebarMenu')
    def parse_main_category(self, response):
        # Кондишен будет True тогда, когда в response будет женская категория.
        rege = re.match(r'http://www.armani.com/subhome.asp\?tskay=\w+&gender=D&season=\w+$', response.url)
        if str(rege) != "None":
            ul_numb = 1
        else:
            ul_numb = 2
        with open('debug_log.txt', 'a')as f:
            f.write(str("HERE ") + str(ul_numb) + '\n' + str(response.url) + '\n' + str(rege) + '\n' + '\n')
        main_category_list = []
        # Тут проверяем, есть ли на главной страници модельной линии (кутюрье) итемы.
        # Если да, отправляем сразу на парсинг итемов.
        nonSubCategory_items_count = int(float(response.xpath('count(//*[@id="elementsContainer"]/div/a)')
                                               .extract_first()))
        with open('debug_log.txt', 'a')as f:
            f.write(str(response.url) + ' ' + str(nonSubCategory_items_count) + '\n')
        if nonSubCategory_items_count != 0:
            valid_url = Request(url=response)
            with open('items_link.txt', 'a') as f:
                f.write(valid_url.url + '\n')
            yield Request(url=valid_url.url, callback=self.parse_item)

        main_category_count = response.xpath('count(//*[@id="sidebarMenu"]/ul/li[%s]/ul/li)' % ul_numb).extract_first()
        for number in range(1, int(float(main_category_count)) + 1):
            sub_sub_count = int(float(response.xpath("count(//*[@id='sidebarMenu']/ul/li[%s]/ul/li[%s]/ul/li)" %
                                                     (ul_numb, number)).extract_first()))
            if sub_sub_count > 1:
                for sub_category in range(1, sub_sub_count + 1):
                    sub_menu = response.xpath('//*[@id="sidebarMenu"]/ul/li[%s]/ul/li[%s]/ul/li[%s]/a' %
                                              (ul_numb, number, sub_category))
                    url = sub_menu.css('a::attr(href)').extract_first()
                    if url[0] == "/":
                        url = DEFAULT_URL + url
                    valid_url = Request(url=url)
                    with open('categorys.txt', 'a') as f:
                        f.write(valid_url.url + '\n')
                    yield Request(url=valid_url.url, callback=self.parse_category)
            sub_menu = response.xpath('//*[@id="sidebarMenu"]/ul/li[%s]/ul/li[%s]/a' % (ul_numb, number))
            url = sub_menu.css('a::attr(href)').extract_first()
            if url[0] == "/":
                url = DEFAULT_URL + url
            self.categorys_list.append(url)
            # Тут походу искуственный редирект. А не просто 301.
            valid_url = Request(url=url)
            with open('categorys.txt', 'a') as f:
                f.write(valid_url.url + '\n')
            main_category_list.append(valid_url)
            yield Request(url=valid_url.url, callback=self.parse_category)

    # Parse Category. The end is near
    def parse_category(self, response):
        last_category_list = []
        item_count = response.xpath('count(//*[@id="elementsContainer"]/div)').extract_first()
        for number in range(2, int(float(item_count)) + 1):
            item_xpath = response.xpath('//*[@id="elementsContainer"]/div[%s]' % number)
            item = item_xpath.css('a::attr(href)').extract_first()
            if item[0] == "/":
                item = DEFAULT_URL + item
            last_category_list.append(item)
            valid_url = Request(url=item)
            with open('items_link.txt', 'a') as f:
                f.write(valid_url.url + '\n')
            yield Request(url=valid_url.url, callback=self.parse_item)

    def parse_nonSubCategory(self, response, items_count):
        for item in range(2, items_count):
            item_xpath = response.xpath('//*[@id="elementsContainer"]/div[%s]' % item)
            item_url = item_xpath.css('a::attr(href)').extract_first()
            if item_url[0] == "/":
                item = DEFAULT_URL + item_url
            valid_url = Request(url=item)
            with open('debug_log.txt', 'a')as f:
                f.write(valid_url)
            yield Request(url=valid_url.url, callback=self.parse_item)

    def parse_item(self, response):
        size_list_xpath = ["//*[@id='sizew_1']/a/text()", "//*[@id='sizew_2']/a/text()", "//li[@id='sizew_3']/a/text()",
                           "//li[@id='sizew_4']/a/text()", "//*[@id='sizew_5']/a/text()", "//*[@id='sizew_6']/a/text()"]
        color_list_xpath = ["//*[@id='color_1']/a/text()", "//*[@id='color_2']/a/text()", "//*[@id='color_3']/a/text()",
                            "//*[@id='color_4']/a/text()", "//*[@id='color_5']/a/text()", "//*[@id='color_6']/a/text()"]
        hxs = Selector(response)
        l = ArmaniLoader(ArmaniItem(), hxs)
        currency = response.xpath('//*[@id="pageContent"]/article/aside/div[3]/div[1]/span[1]/text()').extract_first()
        # http://www.armani.com/fr/armanicom/sac-besace_cod45369108hd.html
        region = None
        if re.match(r'http://www.armani.com/fr/\S+$', response.url):
            region = 'FR'
        elif re.match(r'http://www.armani.com/us/\S+$', response.url):
            region = 'USA'
        l.add_value('region', region)
        l.add_value('currency', currency)
        l.add_xpath('productName', '//*[@id="%s"]/article/aside/h1/text()' % u'pageContent')
        l.add_xpath('priceValue', '//*[@id="%s"]/article/aside/div[3]/div[1]/span[2]/text()' % u'pageContent')
        colors_list = []
        for color in color_list_xpath:
            color_value = response.xpath(color)
            if len(str(color_value)) > 5:
                colors_list.append(color_value.extract_first())

        l.add_value('color', ','.join(colors_list))
        # l.add_xpath('size', '//*[@id="%s"]/li/preceding-sibling::*' % u'elm_5')
        l.add_xpath('descript', '//*[@id="%s"]/article/aside/ul[2]/li[1]/div[2]/text()' % u'pageContent')
        l.add_xpath('sku', '//*[@id="%s"]/article/aside/h3/span[2]/text()' % u'pageContent')
        #
        # l.add_value('id', '%s' % u"null")
        # l.add_value('category', '%s' % u'null')

        final_size_list = []
        for size in size_list_xpath:
            size_value = response.xpath(size)
            if len(str(size_value)) > 5:
                final_size_list.append(size_value.extract_first())
        l.add_value('size', ','.join(final_size_list))
        l.add_value('url', response.url)

        return l.load_item()

# configure_logging()
# runner = CrawlerRunner()
# runner.crawl(ArmaniSpider)
# runner.crawl(ArmaniSpider2)
# d = runner.join()
# d.addBoth(lambda _: reactor.stop())

#reactor.run()
