# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field


class ArmaniItem(scrapy.Item):
    # category = Field()
    # name = Field()
    # id = Field()
    # instock = Field()
    region           = Field()
    productName      = Field()
    currency         = Field()
    priceValue       = Field()
    color            = Field()
    size             = Field()
    sku              = Field()
    url              = Field()
    descript = Field()

