from scrapy.contrib.exporter import CsvItemExporter


class MyCsvItemExporter(CsvItemExporter):
    def __init__(self, *a, **kw):
        kw['delimiter'] = ';'
        super(MyCsvItemExporter, self).__init__(*a, **kw)

